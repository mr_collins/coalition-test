<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use const DIRECTORY_SEPARATOR as DS;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $products = json_decode(Storage::disk('local')->get('product.json'));

       if(!empty($products)){
           $total_values  = array_column($products,'total_value');
           $total = array_sum($total_values);
       }else{
           $total = null;
       }
       $data = [
           'products' => $products,
           'total' => $total
       ];
        return view('welcome', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'product_name' => 'required|string',
            'quantity_in_stock' => 'required|integer',
            'price' => 'required|integer'
        ]);

       $product_json = Storage::disk('local')->exists('product.json');
       if($product_json){
           $product = json_decode(Storage::disk('local')->get('product.json'));
       }

       $date = Carbon::now();
       $input = $request->all();

       $input['date'] = $date;
       $input['total_value'] = $request->price * $request->quantity_in_stock;
       array_push($product, $input);
      $save = Storage::disk('local')->put('product.json', json_encode($product));
      if($save){
          return [
            'status' => true
          ];
      }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $products = json_decode(Storage::disk('local')->get('product.json'));
        abort_unless(is_array($products), 404);
        $id = (integer)$id;
       $product = $products[--$id];
        return [
            'status' => true,
            'product' => $product,
            'key' => $id
        ];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $this->validate($request,[
            'product_name' => 'required|string',
            'quantity_in_stock' => 'required|integer',
            'price' => 'required|integer',
            'key' =>'required|integer'
        ]);

        $id = $request->key;
        $products = json_decode(Storage::disk('local')->get('product.json'));
        abort_unless(is_array($products), 404);
        $id = (integer)$id;
        $product = $products[$id];

        $product->product_name = $request->product_name;
        $product->quantity_in_stock = $request->quantity_in_stock;
        $product->price = $request->price;
        $product->total_value = $request->price * $request->quantity_in_stock;
        json_encode($product);

        Storage::disk('local')->put('product.json', json_encode($products));

        return [
          'status' =>true,
        ];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
