<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ProductController@index')->name('home');
Route::post('/save-product', 'ProductController@store')->name('save-product');
Route::post('/show-product/{id}', 'ProductController@show')->name('show-product');
Route::post('/edit', 'ProductController@edit')->name('edit');