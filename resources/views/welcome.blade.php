<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name', 'Coalition Test') }}</title>
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!--style sheet-->
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!--script-->
        <style>
           body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;

                            }
        </style>
    </head>
    <body>
    <div class="container">
        <div class="alert alert-danger" style="display:none">

            <ul>

                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
            <div id="form" class="row mt-5">
                <form>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Product Name</label>
                        <input type="text" class="form-control" name="product_name" id="product-name">
                    </div>
                    <div class="form-group">
                        <label for="quantity-in-stock">Quantity in Stock</label>
                        <input type="number" class="form-control" id="quantity-in-stock" name="quantity_in_stock">
                    </div>
                    <div class="form-group">
                        <label for="price-per-item">Price Per item</label>
                        <input type="number" class="form-control" id="price-per-item" name="price">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>

        @if(!empty($products))
            <div class="row">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Product Name</th>
                        <th scope="col">Quantity in Stock</th>
                        <th scope="col">Price Per Item</th>
                        <th scope="col">Datetime submitted</th>
                        <th scope="col">Total value number</th>
                        <th scope="col">Edit</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $key => $product)
                    <tr>
                        <th scope="row">{{++$key}}</th>
                        <td>{{$product->product_name}}</td>
                        <td>{{$product->quantity_in_stock}}</td>
                        <td>{{$product->price}}</td>
                        <td>{{$product->date}}</td>
                        <td>{{$product->quantity_in_stock * $product->price}}</td>
                        <td><a href="{{route('show-product', ['id' => $key])}}">edit</a></td>
                    </tr>
                    @endforeach
                    <tr>
                        <td>Total</td>
                        <td>{{$total}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            @endif
        </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Product Name</label>
                            <input type="text" class="form-control" name="product_name" id="modal-product-name">
                        </div>
                        <div class="form-group">
                            <label for="quantity-in-stock">Quantity in Stock</label>
                            <input type="number" class="form-control" id="modal-quantity-in-stock" name="quantity_in_stock">
                        </div>
                        <div class="form-group">
                            <label for="price-per-item">Price Per item</label>
                            <input type="number" class="form-control" id="modal-price-per-item" name="price">

                        </div>
                        <input hidden type="number" name="key" id="modal-key">
                        <div class="align-content-center">
                            <button type="submit" class="btn btn-primary">Submit</button>

                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    </body>
    <script src="https://code.jquery.com/jquery-3.4.0.js"
            integrity="sha256-DYZMCC8HTC+QDr5QNaIcfR7VSPtcISykd+6eSmBW5qo="
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script>
    $(document).ready(function () {
        $('div#form form').submit(function (e) {
            e.preventDefault();
            var form = $(this);
            var attr = "{{route('save-product')}}";
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: attr,
                type: 'POST',
                data:form.serialize(),
                success: function (xhr) {
                    if (xhr.status == true) {
                        location.reload();
                    }
                },
                error: function (xhr,) {
                   var json = $.parseJSON(xhr.responseText);
                    $.each(json.errors, function(key, value){
                        $('.alert-danger').show();
                        $('.alert-danger').append('<p>'+value+'</p>');
                    });

                }
            });
        });
        $('td a').click(function (e) {
            e.preventDefault();
            var attr  = $(this).attr('href');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: attr,
                type: 'POST',
                success: function (xhr) {
                    if (xhr.status == true) {
                        $('#exampleModal').modal('show')

                        $('input[name="product_name"]#modal-product-name').val(xhr.product.product_name);
                        $('input[name="quantity_in_stock"]#modal-quantity-in-stock').val(xhr.product.quantity_in_stock);
                        $('input[name="price"]#modal-price-per-item').val(xhr.product.price);
                        $('input[name="key"]#modal-key').val(xhr.key);
                    }
                },
            });
        });

        $('#exampleModal form').submit(function (e) {
            e.preventDefault();
            var form = $(this);
            console.log(form);
            var attr = "{{route('edit')}}";
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: attr,
                type: 'POST',
                data:form.serialize(),
                success: function (xhr) {
                    if (xhr.status == true) {
                        location.reload();
                    }
                },
            });
        });

    });
</script>
</html>
